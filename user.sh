#!/bin/bash

if [ $# -lt 1 ]; then
	echo "Usage: $0 <output image>.png [geometry]" >&2
	exit 1;
fi

TMP=$1
output="$1.png"
echo "Outputting to image $output, tmp = $TMP"

x=600
y=400

if [ $# -gt 1 ]; then
	x=$2;
	y=$3;
fi

git log -C --numstat --reverse --pretty=format:"%H %an" | perl -e '
	use strict;

	my %users;

	my $line = 1;

	sub add_commit {
		my ($author, $ins, $del) = @_;
		$users{$author} += ($ins - $del);
	}

	my $author = "";
	my $ins = 0;
	my $del = 0;
	while( <> ) {
		if( $_ =~ /^[0-9]+[\t ]+[0-9]+/ ) {
			my ($add, $rem, $file) = split;
			#if( not $file =~ /^devices\/[a-z]+/ ) {
				$ins += $add;
				$del += $rem;
			#}
			next;
		}
		if( $_ =~ /^[0-9a-z]+/ ) {
			my ($commit, $an) = split(/\s+/, $_, 2);
			if( $line != 1 ) {
				&add_commit($author, $ins, $del);
				$ins = 0;
				$del = 0;
			}

			chomp($an);
			$author = $an;
		}

		$line++;
	}

	&add_commit($author, $ins, $del);

	my ($author, $value);
	foreach $author (keys %users) {
		print "$author $users{$author}\n";
	}
	' > $TMP

#lines=$(wc -l $TMP | cut -f1 -d' ')

gnuplot << _EOF_
	set terminal png nocrop medium size $x,$y
	set output '$output'
	plot "$TMP" using 2 with boxes
_EOF_

rm $TMP

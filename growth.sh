#!/bin/bash

if [ $# -lt 1 ]; then
	echo "Usage: $0 <output image>.png [geometry]" >&2
	exit 1;
fi

TMP=$1
output="$1.png"
echo "Outputting to image $output, tmp = $TMP"

x=600
y=400

if [ $# -gt 1 ]; then
	x=$2;
	y=$3;
fi

git log -C --numstat --reverse --pretty=oneline | awk '
	function print_commit() {
		print id" "ins - del;
	}
	BEGIN { id=""; ins=0; del=0; }
	/^[0-9]+[\t ]+[0-9]+/ {
		ins = ins + $1;
		del = del + $2;
		next;
	}
	/^[0-9a-z]+/ {
		if( NR != 1 ) print_commit();
		id = $1;
	}
	END{ print_commit(); }
	' > $TMP

gnuplot << _EOF_
	set terminal png nocrop medium size $x,$y
	set output '$output'
	plot "$TMP" using 2
_EOF_

rm $TMP

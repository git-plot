#!/bin/bash

if [ $# -lt 1 ]; then
	echo "Usage: $0 <output folder> [geometry]" >&2;
	exit 1;
fi

output="$1"
echo "Outputting to image $output"

if [ -e $output ]; then
	echo "Output directory $output already exists" >&2;
	exit 2;
fi

x=600
y=400

if [ $# -gt 1 ]; then
	x=$2;
	y=$3;
fi

git log -C --numstat --reverse --pretty=format:"%H %an" | perl -e '
	use strict;

	my %files;

	my $output = ".gitblaai";
	mkdir($output) or die("Failed to create directory $output. Exiting.\n");

	my $author;
	my $commit;
	my $ncommits = 0;
	sub add_commit {
		my ($file, $ins, $del) = @_;

		my $size;
		if( exists $files{ $file } ) {
			my $prevsize = $files{$file}[ $#{ $files{$file} } ];
#			print "ADD $file prev = $prevsize";
			$size = $prevsize;
		} else {
#			print "NEW $file";

			my $i;
			for($i = 0; $i < $ncommits; ++$i ) {
				push( @{ $files{$file} }, 0 );
			}
			$size = 0;
		}

		$size += ($ins - $del);
#		print " => $size ($commit)\n";
		push( @{ $files{$file} }, $size );
	}

	sub rename_file {
		my ($old, $new) = @_;

		if( not exists $files{ $old } ) {
			print STDERR "$old does not exist\n";
		} else {
			$files{ $new } = $files{ $old };
			delete $files{ $old };
		}

#		print "MV \"$old\" \"$new\"\n";
	}

	sub push_empty {
		my $file;
		foreach $file (keys %files) {
			if( $#{ $files{ $file } } < $ncommits ) {
				my $prevsize = $files{$file}[ $#{ $files{$file} } ];
				push( @{ $files{$file} }, $prevsize );
			}
		}
	}

	while( <> ) {
		if( $_ =~ /^[0-9]+[\t ]+[0-9]+\s+\{[\w\/]+ => [\w\/]+\}\// ) {
			my ($ins, $del, $fromdir, $todir, $file) = split(/[\s\{(=>)\}]+/, $_);
			&rename_file("$fromdir$file", "$todir$file");
		} elsif( $_ =~ /^\d+\s+\d+\s+[\w\/]+\{[\w\.\/]+ => [\w\.\/]+\}/ ) {
			my ($ins, $del, $path, $from, $to) = split(/[\s\{(=>)\}]+/, $_);
			print "RENAME $path,$from,$to\n";
			&rename_file("$path$from", "$path$to");

			my $file = "$path$to";
			&add_commit($file, $ins, $del);
		} elsif( $_ =~ /^[0-9]+[\t ]+[0-9]+/ ) {
			my ($ins, $del, $file, $arrow, $new) = split;
			if( $arrow eq "=>" ) {
				&rename_file($file, $new);
				$file = $new;
			}

			&add_commit($file, $ins, $del);
		} elsif( $_ =~ /^[0-9a-z]+/ ) {
			if( $ncommits != 1 ) {
				&push_empty();
			}

			($commit, $author) = split(/\s+/, $_, 2);
			$ncommits++;
		}
	}

	my $file;
	foreach $file (keys %files) {
		my $filename = $file;
		$filename =~ s/\//_/g;
		open( FILE, ">>$output/$filename" );
		my $i;
		for $i ( 0 .. $#{ $files{$file} } ) {
			print FILE "$file $files{$file}[$i]\n";
		}

	close(FILE)
	}
	' # > $TMP

mv .gitblaai $output

ls $output/ | while read file; do
echo $file
gnuplot << _EOF_
	set terminal png nocrop medium size $x,$y
	set output '$output/$file.png'
	plot "$output/$file" using 2 with points
_EOF_
done
